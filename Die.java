import java.util.Random;
public class Die 
{
    private int faceValue;
    
    private static Random rand = new Random();

    public Die()
    {
        this.faceValue = 1;
    }

    public int getFaceValue()
    {
        return this.faceValue;
    }

    public void roll()
    {
        this.faceValue = rand.nextInt(1,7);
    }

    public String toString()
    {
        return "Facevalue: " + this.faceValue;
    }
}
