public class Jackpot 
{
    public static void main (String[]args)
    {
        System.out.println("Welcome to Jackpot game!!");
        Board board = new Board();
        boolean gameOver = false;
        int numOfTilesClosed = 0;
        while (!(gameOver))
        {
            System.out.println(board);
            boolean checkIfGameOver = board.playATurn();
            if (checkIfGameOver)
            {
                gameOver = true;
            }
            else
            {
                numOfTilesClosed++;
            }
        }
        if (numOfTilesClosed >= 7)
        {
            System.out.println("YOU HAVE WON THE JACKPOT!!! CONGRADULATIONS!!!");
        }
        else
        {
            System.out.println("Sorry you lost! Better luck next time!");
        }
    }    
}
